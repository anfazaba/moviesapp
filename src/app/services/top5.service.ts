import { Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class Top5Service {


  constructor(private http: HttpClient){
  }

  getTop5(){
    return this.http.get("http://www.mocky.io/v2/5dc3c053300000540034757b");
  }


}
