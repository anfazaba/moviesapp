import { Injectable} from '@angular/core';
import * as moment from 'moment';

@Injectable()
export class MoviesService {

  private movies:Movie[] = [
   /* {
      id: 1,
      title: "Titulo 100",
      release: moment("2021-03-17"),
      description: "Descripcion pelicula 1",
      image: "No hay imagen"
    },
    {
      id: 2,
      title: "Titulo 200",
      release: moment("2010-01-01"),
      description: "Descripcion pelicula 2",
      image: "No hay imagen"
    } */
  ];

  constructor() {
  }

  getMovies(){
    return this.movies;
  }

  getMovie(id:string){
    return this.movies.find(movie => movie.id.toString() == id);
  }

  addMovie(movie:Movie){
    if(movie.id == null){
      movie.id = this.findMaxId() + 1;
    }
    this.movies.push(movie);
    return movie.id;
  }

  private findMaxId(){
    let max = Math.max.apply(Math, this.movies.map( movie => movie.id ));
    if(max == -Infinity){
      max = 0;
    }
    return max;
  }

  deleteMovie(movie){
    this.movies = this.movies.filter( filter => filter.id != movie.id);
  }

}

export interface Movie {
  id:number;
  title:string,
  release:moment.Moment,
  description:string,
  image: string
}
