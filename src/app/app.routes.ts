import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from './components/add/add.component';
import { HomeComponent } from './components/home/home.component';
import { Top5Component } from './components/top5/top5.component';

const APP_ROUTES: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'add', component: AddComponent},
  {path: 'top5', component: Top5Component},
  {path: 'movies', component: HomeComponent},
  {path: 'movie/:id', component: HomeComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'home'}

];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
