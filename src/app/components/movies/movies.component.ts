import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Movie, MoviesService } from 'src/app/services/movies.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogBoxComponent } from '../shared/dialog-box/dialog-box.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  movies:Movie[] = [];

  @Output() moviesOutput: EventEmitter<Movie[]>;

  constructor( private _moviesService:MoviesService,
               public dialogo: MatDialog,
               private router: Router ) {
    this.moviesOutput = new EventEmitter();

  }

  ngOnInit() {
    this.movies = this._moviesService.getMovies();
    this.moviesOutput.emit(this.movies);
  }

  delete(movie){
    this.showDialog(movie);
  }

  showDialog(movie:Movie){
    this.dialogo.open(DialogBoxComponent, {
      data: 'Wanna delete the selected movie?'
    })
    .afterClosed()
    .subscribe((confirmado: Boolean) => {
      if (confirmado) {
        this._moviesService.deleteMovie(movie);
        this.router.navigate(['home']);
      }
    });
  }

}
