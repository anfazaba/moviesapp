import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  length: number = 0;
  movies: any[] = [];

  constructor(private _moviesService:MoviesService) {
    this.getMovies();
  }

  ngOnInit(): void {
  }

  getMovies(){
    this.movies = this._moviesService.getMovies();
    this.length = this.movies.length;
  }

  length0(){
    return this.length == 0;
  }

}
