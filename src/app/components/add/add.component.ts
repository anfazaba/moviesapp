import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Movie, MoviesService } from 'src/app/services/movies.service';
import * as moment from 'moment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  movie:Movie = {
    id: null,
    title: "",
    release: null,
    description: "",
    image: ""
  };

  showError:boolean = false;

  constructor(private _moviesService:MoviesService, private router: Router ) { }

  ngOnInit(): void {
  }

  resetMsg(){
    this.showError = false;
  }

  parseDate(dateString: string): moment.Moment {
    if (dateString) {
        return moment(dateString);
    }
    return null;
}

  validateCamps(movie:Movie):boolean{
    return movie.title != null && movie.release !=null && movie.image != null && movie.image != "";
  }

  saveMovie(){
    if(this.validateCamps(this.movie)){
      this.showError = false;
      let idAdded = this._moviesService.addMovie(this.movie);
      this.router.navigate(['/movie', idAdded]);
    } else {
      this.showError = true;
    }
  }

  today(){
    const datepipe: DatePipe = new DatePipe('en-US');
    let formattedDate:string = datepipe.transform(new Date(), 'yyyy-MM-dd')
    return formattedDate;
  }

  convertToBase64(ev){
    let file = ev.target.files[0];
    if(file){
      var reader = new FileReader();
      reader.onload =this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }

  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.movie.image = btoa(binaryString);
   }

}
