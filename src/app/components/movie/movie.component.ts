import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Movie, MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {

  movie:Movie;

  constructor(private activatedRoute: ActivatedRoute,
              private _moviesService: MoviesService) {
    this.activatedRoute.params.subscribe(
      params => {
        this.movie = _moviesService.getMovie(params['id']);
      }
    )
  }

  ngOnInit(): void {
  }

  empty(image:string){
    return image=='No hay imagen' || image == null || image == ""
  }

}
