import { Component, OnInit } from '@angular/core';
import { Top5Service } from 'src/app/services/top5.service';
import * as moment from 'moment';

@Component({
  selector: 'app-top5',
  templateUrl: './top5.component.html',
  styleUrls: ['./top5.component.css']
})
export class Top5Component implements OnInit {

  movies: any[] = [];
  moment: any = moment;

  constructor(private _top5Service:Top5Service) {
     _top5Service.getTop5().subscribe(
      (data: any) => {
        this.movies = data.movies
      }
    );
  }

  ngOnInit(): void {
  }

}
