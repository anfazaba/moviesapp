import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { APP_ROUTING } from './app.routes';
import { AddComponent } from './components/add/add.component';
import { Top5Component } from './components/top5/top5.component';

//Services
import { MoviesService } from './services/movies.service';
import { Top5Service } from './services/top5.service';

import { MoviesComponent } from './components/movies/movies.component';
import { MovieComponent } from './components/movie/movie.component';
import { DialogBoxComponent } from './components/shared/dialog-box/dialog-box.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    AddComponent,
    Top5Component,
    MoviesComponent,
    MovieComponent,
    DialogBoxComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatDialogModule,
    MatButtonModule,
    HttpClientModule
  ],
  providers: [
    MoviesService,
    Top5Service
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    DialogBoxComponent
  ]
})
export class AppModule { }
